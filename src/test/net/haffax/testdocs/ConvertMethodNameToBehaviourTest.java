package net.haffax.testdocs;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class ConvertMethodNameToBehaviourTest {
	@Test
	public void testIfMethodStartsWithTestThatTestIsRemoved() {
		assertThat(TestDocs.convertMethodNameToBehaviour("testX"), equalTo("X"));
		assertThat(TestDocs.convertMethodNameToBehaviour("testXx"), equalTo("Xx"));
		assertThat(TestDocs.convertMethodNameToBehaviour("testTestXx"), equalTo("Test Xx"));
		assertThat(TestDocs.convertMethodNameToBehaviour("testIfMethodStartsWithTestThatTestIsRemoved"),
				equalTo("If Method Starts With Test That Test Is Removed"));
	}

	@Test
	public void testTestMethodNameIsBrokenAtUpperCaseChar() {
		assertThat(TestDocs.convertMethodNameToBehaviour("testTestMethodNameIsBrokenAtUpperCaseChar"),
				equalTo("Test Method Name Is Broken At Upper Case Char"));
	}

	@Test
	public void testConsecutiveUpperCaseCharsAreNotBrokenApart() {
		assertThat(TestDocs.convertMethodNameToBehaviour("testDontBreakAllUPPERCaseChars"),
				equalTo("Dont Break All UPPER Case Chars"));
	}

	@Test
	public void testSingleDigitIsAWord() {
		assertThat(TestDocs.convertMethodNameToBehaviour("test1"), equalTo("1"));
		assertThat(TestDocs.convertMethodNameToBehaviour("test1Test"), equalTo("1 Test"));
		assertThat(TestDocs.convertMethodNameToBehaviour("test1Test2Test"), equalTo("1 Test 2 Test"));
	}

	@Test
	public void testConsecutiveDigitsAreNotBrokenApart() {
		assertThat(TestDocs.convertMethodNameToBehaviour("test99TestBalloons"), equalTo("99 Test Balloons"));
	}

	@Test
	public void testConsecutiveDigitsAreSeperatedFromFollowingAllUpperCaseChars() {
		assertThat(TestDocs.convertMethodNameToBehaviour("test99TESTBalloons"), equalTo("99 TEST Balloons"));
	}
}
