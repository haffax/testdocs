package net.haffax.testdocs;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

public class TestDocs {

	static class TestClassFileVisitor extends SimpleFileVisitor<Path> {

		private List<Path> m_results = new ArrayList<Path>();

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			if (file.toString().endsWith("Test.class")) {
				System.out.println(file.normalize().toString());
				m_results.add(file);
			}
			return FileVisitResult.CONTINUE;
		}

		public Collection<Path> getResults() {
			return m_results;
		}
	}

	static Collection<Path> getTestClassFiles(Path root) {
		TestClassFileVisitor visitor = new TestClassFileVisitor();
		try {
			Files.walkFileTree(root, visitor);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return visitor.getResults();
	}

	static void prettyPrintMethod(Method method) {
		System.out.println("\t" + convertMethodNameToBehaviour(method.getName()));
	}

	static void prettyPrintClass(JavaClass clazz) {
		System.out.println(extractNameOfSystemUnderTest(clazz.getClassName()));
	}

	static String extractNameOfSystemUnderTest(String s) {
		return s.replaceFirst("Test$", "").replaceAll("[A-Za-z0-9_]+\\.", "");
	}

	static String convertMethodNameToBehaviour(String s) {
		return s.replaceAll("(?<=[A-Z])(?=[A-Z][a-z])|(?<=[^A-Z])(?=[A-Z])|(?<=[A-Za-z])(?=[^A-Za-z])", " ")
				.replaceFirst("test ", "");

	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("bad args!");
			System.out.println("usage: java TestDocs <class files directory>");
			System.exit(1);
		}

		Path root = FileSystems.getDefault().getPath(args[0]);
		Collection<Path> classFilePathes = getTestClassFiles(root);

		for (Path path : classFilePathes) {
			try {
				ClassParser parser = new ClassParser(path.toString());
				JavaClass clazz = parser.parse();
				prettyPrintClass(clazz);
				for (Method method : clazz.getMethods()) {
					if (method.getName().startsWith("test")) {
						prettyPrintMethod(method);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
